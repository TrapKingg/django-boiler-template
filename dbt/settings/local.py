from .base import *

DEBUG = config('DEBUG', default=False, cast=bool)

#Here you can config the email options

# config the caching methods

# Django debuh toolbar configuration
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]
INSTALLED_APPS += ['debug_toolbar',]

INTERNAL_IPS = ['127.0.0.1', ]
